# What

This is a demo project to reproduce the eclipse bug [532464 views/parts cannot be moved anymore on right display in a 2nd window](https://bugs.eclipse.org/bugs/show_bug.cgi?id=532464). 

## goal 
For now the sole goal is to provide a minimal RCP App where the behavior is reproducible.

## Bug Steps for reproduction
I repeat the steps here to have them at my finger tips and also to be able to just adjust them as new light comes into the matter (as bugzilla wont let u edit comments).

1. Start eclipse on (right) display (Window 1, W1)
2. Goto workbench
3. Drag any view to another stack and drop it -> works
4. Create 2nd window W2 on right display (Menu: Window/New Window)
5. Maximize both
6. In W2: move a view as above -> works 
7. Goto W1
8. move a view as above
 
   -> dosnt work (sometimes u have to repeat step 6 a few times, so this fails)
9. de-maximze W2 and goto W1
1. DnD views

   -> works again

## RCP Setup

I provided a target platform definition which works against a [downloaded Oxygen 3a win64](https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/oxygen/3a/eclipse-committers-oxygen-3a-win32-x86_64.zip).
Use it and then start the RCP App.
